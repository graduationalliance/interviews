# README #

This is the programming challenge for Graduation Alliance. Please fork this repository and submit your completed project as a pull request. Node creates a pretty deep folder structure, so it is best to check this out close to the root of your drive (c:\src for example).  Because of this, it is possible you will not be able to delete the node_modules folder directly. To do this, you will either need a file manager that can handle long file paths (7-zip is a good choice) or install rimraf (npm install rimraf && rimraf node_modules).

### How do I get set up? ###

This project should be ready to go, simply open it and hit run. This will download the nuget and node packages, then compile and run it.
The first build will take a long (no seriously long.  Node is slow) time.  Each build after will be faster, but node is invoked on each build. To build manually, remove the pre-build event that triggers the node build (you can use the following command in the web directory: npm run watch)

### What is the task? ###

The Web project has several TODOs within it, search for them and complete them all. The result is that a person can see and search for the students that are associated with a teacher. The following mockup is the page you are tasked with building.

![mockup.jpg](https://bitbucket.org/repo/kMMgMdp/images/2497106601-mockup.jpg)