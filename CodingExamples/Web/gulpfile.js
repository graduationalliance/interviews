"use strict";

// Setup imports
var gulp = require('gulp');
var del = require('del');

// Externals
var scripts = require('./gulp/scripts');
var config = require('./gulp/config');
var styles = require('./gulp/styles');
var icons = require('./gulp/icons');
var html = require('./gulp/html');

// Config Tasks
var watch = false;

gulp.task('set-watch', function () {
    watch = true;
});

// Primary tasks
gulp.task('js-compile', function (done) {
    if (watch) scripts.watch().then(function () { done(); });
    else scripts.build().then(function () { done(); });
});

gulp.task('css-compile', function (done) {
    styles.build().then(function () { done(); });
});

gulp.task('html-compile', function (done) {
    html.build().then(function () { done(); });
});

gulp.task('icons-compile', function (done) {
    icons.build().then(function () { done(); });
});

// Entry points
gulp.task('default', ['icons-compile', 'html-compile', 'css-compile', 'js-compile']);

gulp.task('watch', ['set-watch', 'icons-compile', 'html-compile', 'css-compile'], function () {
    gulp.watch(config.scssSource + config.scssSourceFiles, ['css-compile']);
    gulp.watch(config.htmlSource + config.htmlSourceFiles, ['html-compile']);
    gulp.watch(config.contentSource + config.contentSourceFiles, ['content-compile']);
    gulp.start('js-compile')
});

gulp.task('clean', function (cb) {
    del([config.output + '/**', '!' + config.output])
        .then(function (paths) {
            console.log('Output cleaned, ' + paths.length + ' items removed');
            cb();
        });
});