﻿export default (app: angular.IModule) => {
	app.config(($locationProvider: angular.ILocationProvider) => {
		$locationProvider.html5Mode(true);
	});

	app.config((
		$stateProvider: angular.ui.IStateProvider,
		$urlRouterProvider: angular.ui.IUrlRouterProvider,
		$urlMatcherFactoryProvider: angular.ui.IUrlMatcherFactory
	) => {
		$urlMatcherFactoryProvider.caseInsensitive(true);
		$urlMatcherFactoryProvider.strictMode(false);

		function valToString(val) { return val != null ? val.toString() : val; }
		function valFromString(val) { return val != null ? val.toString() : val; }
		function regexpMatches(val) { /*jshint validthis:true */ return this.pattern.test(val); }
		$urlMatcherFactoryProvider.type("NonURIEncoded", {
			encode: valToString,
			decode: valFromString,
			is: regexpMatches
		});

		$urlRouterProvider.otherwise(($injector, $location) => {
			return '/teachers';
        });

        $stateProvider.state('hello-world', {
            url: '/hello/world',
            templateUrl: '/dist/app/pages/helloworld/helloworld.html',
        }).state('hello-universe', {
            url: '/hello/universe',
            templateUrl: '/dist/app/pages/helloworld/hellouniverse.html',
		}).state('teachers', {
			url: '/teachers',
			templateUrl: '/dist/app/pages/teachers/teachers.html',
		}).state('assignedstudents', {
			url: '/teachers/:teacherId/assignedstudents',
			templateUrl: '/dist/app/pages/assignedstudents/assignedstudents.html',
		});
	});
}