﻿import * as AppDefinition from "./appdefinition";

export default class MainModule {
	public static ModuleName: string = "MainModule";

	public GetModuleName(): string {
		return MainModule.ModuleName;
	}

	constructor(angular: angular.IAngularStatic) {
		var app = angular.module(MainModule.ModuleName, AppDefinition.moduleDependencies);
		AppDefinition.initializer(app, angular);
	}
}