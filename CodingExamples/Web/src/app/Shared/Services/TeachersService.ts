﻿export default class TeachersService implements ce.ITeachersService {
	static $inject = [
		"$http"
	];

	constructor(
		public $http: angular.IHttpService,
	) {
	}

	public getAll = (): angular.IPromise<Array<ce.ITeacherModel>> => {
		return this.$http.get('/api/teachers').then((response) => {
			return response.data;
		});
	}

	public get = (teacherId: number): angular.IPromise<ce.ITeacherModel> => {
		return this.$http.get('/api/teachers/' + teacherId).then((response) => {
			return response.data;
		});
	}
}