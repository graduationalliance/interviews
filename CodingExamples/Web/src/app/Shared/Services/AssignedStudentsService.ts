﻿export default class AssignedStudentsService implements ce.IAssignedStudentsService {
	static $inject = [
		"$http"
	];

	constructor(
		public $http: angular.IHttpService,
	) {
	}

	public get = (): angular.IPromise<Array<ce.IAssignedStudentModel>> => {
		// Todo, fetch the data from the appropriate web api endpoint
		throw 'Not Implemented';
	}

	public search = (teacherId: number, args: ce.IAssignedStudentSearchArguments): angular.IPromise<Array<ce.IAssignedStudentModel>> => {
		// Todo, fetch the data from the appropriate web api endpoint
		throw 'Not Implemented';
	}
}