﻿export default class HelloWorldLinkDirective implements angular.IDirective {
	scope = {
		linkName: '=',
		innerContent: '@'
	};

	link = (scope) => {
		scope.text = scope.innerContent;
	}

	replace = true;
	restrict = "E";
	templateUrl = "/dist/app/shared/directives/helloworldlink/helloworldlink.html";

	static factory(): angular.IDirectiveFactory {
		var directive = function (
		) {
			return new HelloWorldLinkDirective();
		}
		return directive;
	}
}