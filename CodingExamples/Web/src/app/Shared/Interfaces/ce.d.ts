﻿declare namespace ce {
	interface ITeachersService {
		getAll: () => angular.IPromise<Array<ITeacherModel>>;
		get: (teacherId: number) => angular.IPromise<ITeacherModel>;
	}

	interface ITeacherModel {
		FirstName: string;
		LastName: string;
		TeacherId: number;
		UserId: number;
	}

	interface IAssignedStudentsService {
		get: (teacherId: number) => angular.IPromise<Array<IAssignedStudentModel>>;
		search: (teacherId: number, args: IAssignedStudentSearchArguments) => angular.IPromise<Array<IAssignedStudentModel>>;
	}

	interface IAssignedStudentSearchArguments {
		FirstName?: string;
		LastName?: string;
		GradeLevel?: number;
		Birthdate?: Date;
		BirthdateGreater?: boolean;
	}

	interface IAssignedStudentModel {
		FirstName: string;
		LastName: string;
		ClassRoomId: number;
		GradeLevel: number;
		Birthdate: string;
		TeacherId: number;
		StudentId: number;
	}
}