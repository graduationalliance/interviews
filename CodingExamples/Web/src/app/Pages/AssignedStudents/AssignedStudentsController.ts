﻿export default class AssignedStudentsController {
	static $inject = [
		'$scope',
		'$state',
	];

	constructor(
		protected $scope: angular.IScope,
		protected $state: angular.ui.IStateService
	) {
		var teacherId = $state.params['teacherId'];

		// TODO, import the TeachersService, use that to fetch the teacher's data for their name

		// Next, import the AssignedStudentsService and use that to build the search and results for this teacher
	}
}