﻿export default class HelloWorldController {
	static $inject = [
		'$scope',
	];

	public world: string = "World";

	constructor(
		protected $scope: angular.IScope
	) {
	}
}