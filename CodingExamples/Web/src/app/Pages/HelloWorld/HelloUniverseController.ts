﻿export default class HelloUniverseController {
	static $inject = [
		'$scope',
	];

	public universe: string = "Universe";
	public worldState: string = "hello-world";

	constructor(
		protected $scope: angular.IScope,
	) {
	}

	public myCoolFunction = (isCool: boolean): string => {
		return this.universe;
	}
}