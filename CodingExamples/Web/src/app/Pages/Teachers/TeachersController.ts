﻿export default class TeachersController {
	static $inject = [
		'$scope',
		'TeachersService',
	];

	public teachers: Array<ce.ITeacherModel>;

	constructor(
		protected $scope: angular.IScope,
		protected TeachersService: ce.ITeachersService
	) {
		TeachersService.getAll().then((teachers) => {
			this.teachers = teachers;
		});
	}
}