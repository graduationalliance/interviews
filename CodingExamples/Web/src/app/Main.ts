﻿import MainModule from "./mainmodule";

export default class Main {
	public static AppName: string = "Main";
	public deferred: any;

	public GetAppName(): string {
		return Main.AppName;
	}

	public init() {
		return this.deferred.promise();
	}

	constructor(angular: angular.IAngularStatic) {
		this.deferred = jQuery.Deferred();

		new MainModule(angular);
		let dependencies = [
			MainModule.ModuleName
		];

		let app = angular.module(Main.AppName, dependencies);

		this.deferred.resolve();
	}
}