﻿// Controllers
import HelloUniverseController from "./pages/helloworld/hellouniversecontroller";
import HelloWorldController from "./pages/helloworld/helloworldcontroller";
import TeachersController from "./pages/teachers/teacherscontroller";
import AssignedStudentsController from "./pages/assignedstudents/assignedstudentscontroller";

export default (app: angular.IModule) => {
	//
	// Controllers
	//
	app.controller('HelloUniverseController', HelloUniverseController);
	app.controller('HelloWorldController', HelloWorldController);
	app.controller('TeachersController', TeachersController);
	app.controller('AssignedStudentsController', AssignedStudentsController);
}