﻿// Services

import TeachersService from "./shared/services/teachersservice";
import AssignedStudentsService from "./shared/services/assignedstudentsservice";

export default (app: angular.IModule) => {
	//
	// Services
	//
	app.service('TeachersService', TeachersService);
	app.service('AssignedStudentsService', AssignedStudentsService);
}