﻿import Controllers from "./appdefinition.controllers";
import Directives from "./appdefinition.directives";
import Services from "./appdefinition.services";
import Routing from "./appdefinition.routing";

export var moduleDependencies = [
	'ngAnimate',
	'ngResource',
	'ngSanitize',
	'ui.router',
];

export var initializer = (app: angular.IModule, angular: angular.IAngularStatic) => {
	Controllers(app);
	Directives(app);
	Services(app);
	Routing(app);
}