﻿// Directives
import HelloWorldLinkDirective from "./shared/directives/helloworldlink/helloworldlinkdirective";

export default (app: angular.IModule) => {
	//
	// Directives
	//
	app.directive('helloWorldLink', HelloWorldLinkDirective.factory());
}