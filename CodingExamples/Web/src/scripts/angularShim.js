﻿var jquery = require('jquery');
var realAngular = require('../../node_modules/angular/angular');

(function (global, factory) {
    factory(global);
})(typeof window !== "undefined" ? window : this, function (window, noGlobal) {
    let __internalAngular = window.angular;

    if (typeof define === "function" && define.amd) {
        define("angular", [], function () {
            return __internalAngular;
        });
    }

    return __internalAngular;
});