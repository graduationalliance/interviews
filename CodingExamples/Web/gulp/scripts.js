﻿'use strict';

var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var WebpackNotifierPlugin = require('webpack-notifier');
var config = require('./config');

var webpackConfig = require('../webpack.config.js');

function buildProduction(done) {
    // modify some webpack config options
    var myProdConfig = Object.create(webpackConfig);

    myProdConfig.plugins = myProdConfig.plugins.concat(
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            mangle: false,
            compress: {
                warnings: true
            }
        })
    );

    // run webpack
    webpack(myProdConfig, function (err, stats) {
        if (err) { throw new gutil.PluginError('webpack:build', err); }
        gutil.log('[webpack:build]', stats.toString({
            colors: true
        }));

        if (done) { done(); }
    });
}

function createDevCompiler(shouldWatch) {
    // show me some sourcemap love people
    var myDevConfig = Object.create(webpackConfig);
    myDevConfig.devtool = 'inline-source-map';

    if (shouldWatch) {
        myDevConfig.cache = true;
    }

    myDevConfig.plugins = myDevConfig.plugins.concat(
      new WebpackNotifierPlugin({ title: 'Webpack build', excludeWarnings: true })
    );

    // create a single instance of the compiler to allow caching
    return webpack(myDevConfig);
}

function buildDevelopment(done, devCompiler) {
    // run webpack
    devCompiler.run(function (err, stats) {
        if (err) { throw new gutil.PluginError('webpack:build-dev', err); }
        gutil.log('[webpack:build-dev]', stats.toString({
            chunks: false, // dial down the output from webpack (it can be noisy)
            colors: true
        }));

        if (done) { done(); }
    });
}

function bundle(options) {
    let devCompiler;

    function build(done) {
        if (options.dev) {
            console.log('Starting Typescript build...');
            buildDevelopment(done, devCompiler);
        } else {
            buildProduction(done);
        }
    }

    if (options.dev) {
        devCompiler = createDevCompiler(options.shouldWatch);
    }

    if (options.shouldWatch) {
        gulp.watch(config.tsSource + config.tsSourceFiles, function () { build(); });
    }

    return new Promise(function (resolve, reject) {
        build(function (err) {
            if (err) {
                reject(err);
            } else {
                resolve('webpack built');
            }
        });
    });
}

module.exports = {
    prod: function () { return bundle({ shouldWatch: false, dev: false }); },
    build: function () { return bundle({ shouldWatch: false, dev: true }); },
    watch: function () { return bundle({ shouldWatch: true, dev: true }); }
};