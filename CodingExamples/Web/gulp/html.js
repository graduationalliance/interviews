﻿"use strict";

// Setup imports
var gulp = require('gulp');
var promise = require('gulp-promise');
var htmlmin = require('gulp-htmlmin');
var validate = require('gulp-html-angular-validate');
var gutil = require('gulp-util');

var config = require('./config');

function build(options, done) {
    try {
        var options = {
            tmplext: '.html',
            customattrs: ['*'],
            customtags: ['*'],
            doctype: 'HTML5',
            relaxerror: [
                'Empty heading.',
                'This document appears to be written in English. Consider adding “lang="en"” (or variant) to the “html” start tag.',
            ],
            reportFn: function (fileFailures) {
                for (var i = 0; i < fileFailures.length; i++) {
                    var fileResult = fileFailures[i];
                    gutil.log(fileResult.filepath);
                    for (var j = 0; j < fileResult.errors.length; j++) {
                        var err = fileResult.errors[j];
                        if (err.line !== undefined) {
                            gutil.log('[line' + err.line + ', col: ' + err.col + '] ' + err.msg);
                        } else {
                            gutil.log(err.msg);
                        }
                    }
                }
            }
        };

        let htmlStream = gulp
            .src(config.htmlSource + config.htmlSourceFiles)
            //.pipe(validate(options)) // enable when you want to lint html
            .pipe(htmlmin({
                //collapseWhitespace: true, // needs further exploration first
                removeComments: true,
            }))
            .pipe(gulp.dest(config.output + '/app/'));

        var streams = [
            htmlStream,
        ];

        var donePromise = new promise();
        donePromise.makePromises(streams, function (err) {
            done(err);
        });

        for (var i = 0; i < streams.length; i++) {
            streams[i].pipe(donePromise.deliverGulpPromise(streams[i]));
        }
    } catch (ex) {
        console.log(ex);
    }
}

function bundle(options) {
    return new Promise(function (resolve, reject) {
        build(options, function (err) {
            if (err) { reject(err); }
            else { resolve('html built'); }
        });
    });
}

module.exports = {
    prod: function () { return bundle({ dev: false }); },
    build: function () { return bundle({ dev: true }); },
};