﻿module.exports = {
    cssSource: './src/css',
    cssSourceFiles: '/**/*.css',

    tsSource: './src/app',
    tsSourceFiles: '/**/*.ts',

    htmlSource: './src/app',
    htmlSourceFiles: '/**/*.html',

    scriptsSource: './src/scripts',
    scriptsSourceFiles: '/**/*.js',

    contentSource: './src/content',
    contentSourceFiles: '/**/*.*',

    bowerDir: './bower_components',
    nodeDir: './node_modules',
    source: './src',
    output: './dist',
    tsDefinitions: './typings'
}