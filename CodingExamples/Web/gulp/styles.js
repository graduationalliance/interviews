﻿'use strict';

var gulp = require('gulp');
var promise = require('gulp-promise');
var gutil = require('gulp-util');

var config = require('./config');

function gatherCss(done) {
	let htmlStream = gulp
		.src(config.cssSource + config.cssSourceFiles)
		.pipe(gulp.dest(config.output + '/css/'));

	done();
}

function bundle() {
	return new Promise(function (resolve, reject) {
		gatherCss(function (err) {
			if (err) { reject(err); }
			else { resolve('css built'); }
		});
	});
}

module.exports = {
	build: function () { return bundle(); },
};