﻿"use strict";

// Setup imports
var gulp = require('gulp');
var flatten = require('gulp-flatten');
var promise = require('gulp-promise');

var config = require('./config');

function build(options, done) {
    try {
        var fontawesome = gulp.src(config.nodeDir + '/font-awesome/fonts/*.*')
            .pipe(flatten())
            .pipe(gulp.dest(config.output + '/fonts/fontawesome'));

        var bootstrap = gulp.src(config.nodeDir + '/bootstrap-sass/assets/fonts/bootstrap/*.*')
            .pipe(flatten())
            .pipe(gulp.dest(config.output + '/fonts/bootstrap'));

        var streams = [
            fontawesome,
            bootstrap
        ];

        var donePromise = new promise();
        donePromise.makePromises(streams, function (err) {
            done(err);
        });

        for (var i = 0; i < streams.length; i++) {
            streams[i].pipe(donePromise.deliverGulpPromise(streams[i]));
        }
    } catch (ex) {
        console.log(ex);
    }
}

function bundle(options) {
    return new Promise(function (resolve, reject) {
        build(options, function (err) {
            if (err) { reject(err); }
            else { resolve('icons built'); }
        });
    });
}

module.exports = {
    prod: function () { return bundle({ dev: false }); },
    build: function () { return bundle({ dev: true }); },
};