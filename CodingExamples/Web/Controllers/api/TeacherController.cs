﻿using Data;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Web.Models;
using System.Data.Entity;


namespace Web.Controllers.api
{
	[RoutePrefix("api/teachers")]
	public class TeacherController : ApiController
	{
		private EducationContainer _ctx;

		public TeacherController(EducationContainer ctx)
		{
			_ctx = ctx;
		}

		[HttpGet]
		[Route]
		[ResponseType(typeof(List<TeacherModel>))]
		public IHttpActionResult Get()
		{
			var teachers = _ctx.Teachers
				.Select(x => new TeacherModel
				{
					FirstName = x.User.FirstName,
					LastName = x.User.LastName,
					TeacherId = x.teacherid,
					UserId = x.User.Id,
				}).ToList();

			return Ok(teachers);
		}

		[HttpGet]
		[Route("{teacherId}")]
		[ResponseType(typeof(TeacherModel))]
		public IHttpActionResult Get(int teacherId)
		{
			var teacher = _ctx.Teachers
				.Where(x => x.teacherid == teacherId)
				.Select(x => new TeacherModel
				{
					FirstName = x.User.FirstName,
					LastName = x.User.LastName,
					TeacherId = x.teacherid,
					UserId = x.User.Id,
				}).FirstOrDefault();

			return Ok(teacher);
		}
	}
}