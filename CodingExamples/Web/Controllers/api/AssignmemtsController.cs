﻿using Data;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using Web.Models;
using System.Data.Entity;
using System.Linq;

namespace Web.Controllers.api
{
	[RoutePrefix("api")]
	public class AssignmemtsController : ApiController
	{
		private EducationContainer _ctx;

		public AssignmemtsController()
		{
			_ctx = new EducationContainer();
		}

		[HttpGet]
		[Route("teachers/{teacherId}/students")]
		[ResponseType(typeof(List<AssignedStudentModel>))]
		public IHttpActionResult Get(int teacherId)
		{
			// Todo, select out the list of AssignedStudentModel that are associated with the teacher
			throw new NotImplementedException();
		}

		[HttpPost]
		[Route("teachers/{teacherId}/students")]
		[ResponseType(typeof(List<AssignedStudentModel>))]
		public IHttpActionResult Search(int teacherId, [FromBody]AssignedStudentSearchArguments arguments)
		{
			// Todo, select out the list of AssignedStudentModel that are associated with the teacher filtered by the
			// search arguments. These search arguments should all be optional and interchangeable.
			throw new NotImplementedException();
		}
	}
}