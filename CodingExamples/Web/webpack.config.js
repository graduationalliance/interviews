/* eslint-disable no-var, strict, prefer-arrow-callback */
'use strict';

var path = require('path');
var config = require('./gulp/config');
var webpack = require('webpack');

module.exports = {
	entry: {
		// The entry point of the application
		vendor: [
			// Major dependencies
			'jquery',
			'angularShim',

			// Minor dependencies
			'bootstrap-sass',
			'angular-animate',
			'angular-sanitize',

			// No dependencies
			'angular-ui-router',
			'angular-resource',
			'angular-route',
			'linq',
		],
		app: path.resolve(config.tsSource, 'AppRoot.ts'),
	},

	// Where the output of our compilation ends up
	output: {
		path: path.resolve(__dirname, config.output + '/scripts'),
		filename: '[name].js',
		chunkFilename: "[name].[id].js"
	},

	module: {
		rules: [{
			test: /\.ts$/,
			exclude: /node_modules/,
			loader: 'ts-loader'
		},
		{
			test: require.resolve('jquery'),
			loader: 'expose-loader?jQuery!expose-loader?$'
		}]
	},
	resolve: {
		modules: [
			path.join(__dirname),
			"node_modules"
		],
		alias: {
			angularShim: path.resolve(__dirname, "src/scripts/angularShim"),
			linq: path.resolve(__dirname, "src/scripts/thirdparty/linq.min.js"),
		},
		extensions: ['.webpack.js', '.web.js', '.ts', '.tsx', '.js']
	},
	plugins: [
		new webpack.NormalModuleReplacementPlugin(
			/^angular$/,
			require.resolve(path.resolve(__dirname, "src/scripts/angularShim"))
		),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery",
			"window.jQuery": "jquery",
			angular: 'angularShim',
			Enumerable: 'linq',
		}),
		new webpack.optimize.CommonsChunkPlugin({
			names: ["vendor"],
			minChunks: Infinity
		})
	],
};