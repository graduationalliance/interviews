﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App_Start.Seeds
{
	public class TeacherSeed
	{
		public static TeacherSeed JaneTeacher = new TeacherSeed(-1);
		public static TeacherSeed JohnTeacher = new TeacherSeed(-1);
		public static TeacherSeed JerryTeacher = new TeacherSeed(-1);
		public static TeacherSeed JennyTeacher = new TeacherSeed(-1);

		public static void Populate(EducationContainer ctx)
		{
			JaneTeacher.UserId = UserSeed.JaneTeacher.Id;
			JohnTeacher.UserId = UserSeed.JohnTeacher.Id;
			JerryTeacher.UserId = UserSeed.JerryTeacher.Id;
			JennyTeacher.UserId = UserSeed.JennyTeacher.Id;

			EnsureTeacherExists(ctx, JaneTeacher);
			EnsureTeacherExists(ctx, JohnTeacher);
			EnsureTeacherExists(ctx, JerryTeacher);
			EnsureTeacherExists(ctx, JennyTeacher);
		}

		private static void EnsureTeacherExists(EducationContainer ctx, TeacherSeed user)
		{
			var theTeacher = ctx.Teachers.Where(u => u.userid == user.UserId).FirstOrDefault();

			if (theTeacher == null)
			{
				theTeacher = ctx.Teachers.Create();
				theTeacher.userid = user.UserId;
				ctx.Teachers.Add(theTeacher);
				ctx.SaveChanges();
			}

			user.TeacherId = theTeacher.teacherid;
		}

		public TeacherSeed(int userId)
		{
			UserId = userId;
		}

		public int TeacherId { get; set; }
		public int UserId { get; set; }

	}
}