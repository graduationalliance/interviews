﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App_Start.Seeds
{
	public class StudentSeed
	{
		public static StudentSeed VictorStudent = new StudentSeed(-1);
		public static StudentSeed RachaelStudent = new StudentSeed(-1);
		public static StudentSeed TracyStudent = new StudentSeed(-1);
		public static StudentSeed YolandaStudent = new StudentSeed(-1);
		public static StudentSeed EnriqueStudent = new StudentSeed(-1);
		public static StudentSeed JacquelineStudent = new StudentSeed(-1);
		public static StudentSeed EricStudent = new StudentSeed(-1);
		public static StudentSeed EdithStudent = new StudentSeed(-1);
		public static StudentSeed MiguelStudent = new StudentSeed(-1);
		public static StudentSeed MartaStudent = new StudentSeed(-1);
		public static StudentSeed FrankStudent = new StudentSeed(-1);
		public static StudentSeed IrvinStudent = new StudentSeed(-1);

		public static void Populate(EducationContainer ctx)
		{
			VictorStudent.UserId = UserSeed.VictorStudent.Id;
			RachaelStudent.UserId = UserSeed.RachaelStudent.Id;
			TracyStudent.UserId = UserSeed.TracyStudent.Id;
			YolandaStudent.UserId = UserSeed.YolandaStudent.Id;
			EnriqueStudent.UserId = UserSeed.EnriqueStudent.Id;
			JacquelineStudent.UserId = UserSeed.JacquelineStudent.Id;
			EricStudent.UserId = UserSeed.EricStudent.Id;
			EdithStudent.UserId = UserSeed.EdithStudent.Id;
			MiguelStudent.UserId = UserSeed.MiguelStudent.Id;
			MartaStudent.UserId = UserSeed.MartaStudent.Id;
			FrankStudent.UserId = UserSeed.FrankStudent.Id;
			IrvinStudent.UserId = UserSeed.IrvinStudent.Id;

			EnsureStudentExists(ctx, VictorStudent);
			EnsureStudentExists(ctx, RachaelStudent);
			EnsureStudentExists(ctx, TracyStudent);
			EnsureStudentExists(ctx, YolandaStudent);
			EnsureStudentExists(ctx, EnriqueStudent);
			EnsureStudentExists(ctx, JacquelineStudent);
			EnsureStudentExists(ctx, EricStudent);
			EnsureStudentExists(ctx, EdithStudent);
			EnsureStudentExists(ctx, MiguelStudent);
			EnsureStudentExists(ctx, MartaStudent);
			EnsureStudentExists(ctx, FrankStudent);
			EnsureStudentExists(ctx, IrvinStudent);

		}

		private static void EnsureStudentExists(EducationContainer ctx, StudentSeed user)
		{
			var theStudent = ctx.Students.Where(u => u.userid == user.UserId).FirstOrDefault();

			if (theStudent == null)
			{
				theStudent = ctx.Students.Create();
				theStudent.userid = user.UserId;
				theStudent.gradelevel = new Random().Next(7, 12);
				ctx.Students.Add(theStudent);
				ctx.SaveChanges();
			}

			user.StudentId = theStudent.studentid;
		}

		public StudentSeed(int userId)
		{
			UserId = userId;
		}

		public int StudentId { get; set; }
		public int UserId { get; set; }

	}
}