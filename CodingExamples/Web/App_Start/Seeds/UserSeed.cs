﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Web.App_Start.Seeds
{
	public class UserSeed
	{
		public static UserSeed JaneTeacher = new UserSeed("Jane", "Teacher", DateTimeOffset.Parse("06/01/1980"));
		public static UserSeed JohnTeacher = new UserSeed("John", "Teacher", DateTimeOffset.Parse("10/01/1982"));
		public static UserSeed JerryTeacher = new UserSeed("Jerry", "Teacher", DateTimeOffset.Parse("05/05/1979"));
		public static UserSeed JennyTeacher = new UserSeed("Jenny", "Teacher", DateTimeOffset.Parse("07/11/1950"));

		public static UserSeed VictorStudent = new UserSeed("Victor", "Student", DateTimeOffset.Parse("07/18/2010"));
		public static UserSeed RachaelStudent = new UserSeed("Racheal", "Park", DateTimeOffset.Parse("11/14/2010"));
		public static UserSeed TracyStudent = new UserSeed("Tracy", "Gonzales", DateTimeOffset.Parse("11/14/2010"));
		public static UserSeed YolandaStudent = new UserSeed("Yolanda", "Shaw", DateTimeOffset.Parse("1/14/2010"));
		public static UserSeed EnriqueStudent = new UserSeed("Enrique", "Shelton", DateTimeOffset.Parse("12/16/2010"));
		public static UserSeed JacquelineStudent = new UserSeed("Jacqueline", "Nash", DateTimeOffset.Parse("07/4/2011"));
		public static UserSeed EricStudent = new UserSeed("Eric", "Coleman", DateTimeOffset.Parse("09/07/2012"));
		public static UserSeed EdithStudent = new UserSeed("Edith", "Dennis", DateTimeOffset.Parse("01/16/2010"));
		public static UserSeed MiguelStudent = new UserSeed("Miguel", "Hill", DateTimeOffset.Parse("12/23/2010"));
		public static UserSeed MartaStudent = new UserSeed("Marta", "Vaughn", DateTimeOffset.Parse("05/15/2011"));
		public static UserSeed FrankStudent = new UserSeed("Frank", "Oliver", DateTimeOffset.Parse("07/16/2011"));
		public static UserSeed IrvinStudent = new UserSeed("Irvin", "Fuller", DateTimeOffset.Parse("03/14/2010"));

		public static Dictionary<UserSeed, List<UserSeed>> Associations = new Dictionary<UserSeed, List<UserSeed>>()
		{
			{ JaneTeacher, new List<UserSeed>(){VictorStudent, RachaelStudent,TracyStudent,YolandaStudent,EnriqueStudent } },
			{ JohnTeacher, new List<UserSeed>(){VictorStudent, EnriqueStudent, JacquelineStudent, EricStudent, EdithStudent } },
			{ JerryTeacher, new List<UserSeed>(){ MiguelStudent, MartaStudent, FrankStudent } },

		};

		public static void Populate(EducationContainer ctx)
		{
			EnsureUserExists(ctx, JaneTeacher);
			EnsureUserExists(ctx, JohnTeacher);
			EnsureUserExists(ctx, JerryTeacher);
			EnsureUserExists(ctx, JennyTeacher);

			EnsureUserExists(ctx, VictorStudent);
			EnsureUserExists(ctx, RachaelStudent);
			EnsureUserExists(ctx, TracyStudent);
			EnsureUserExists(ctx, YolandaStudent);
			EnsureUserExists(ctx, EnriqueStudent);
			EnsureUserExists(ctx, JacquelineStudent);
			EnsureUserExists(ctx, EricStudent);
			EnsureUserExists(ctx, EdithStudent);
			EnsureUserExists(ctx, MiguelStudent);
			EnsureUserExists(ctx, MartaStudent);
			EnsureUserExists(ctx, FrankStudent);
			EnsureUserExists(ctx, IrvinStudent);

		}

		private static void EnsureUserExists(EducationContainer ctx, UserSeed user)
		{
			var theUser = ctx.Users.Where(u => u.Username == user.Username).FirstOrDefault();

			if (theUser == null)
			{
				theUser = ctx.Users.Create();
				theUser.Username = user.Username;
				theUser.FirstName = user.FirstName;
				theUser.LastName = user.LastName;

				ctx.Users.Add(theUser);
				ctx.SaveChanges();
			}

			user.Id = theUser.Id;
		}

		public UserSeed(string firstName, string lastName, DateTimeOffset birthdate)
		{
			Username = $"{firstName}{lastName}";
			FirstName = firstName;
			LastName = lastName;
			Birthdate = birthdate;
		}

		public string Username { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTimeOffset Birthdate { get; set; }
		public int Id { get; set; }
	}
}