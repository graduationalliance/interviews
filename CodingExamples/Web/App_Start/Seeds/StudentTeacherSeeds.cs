﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.App_Start.Seeds
{
	public class StudentTeacherSeeds
	{
		public static void Populate(EducationContainer ctx)
		{
			foreach (var mapping in UserSeed.Associations)
			{
				foreach (var student in mapping.Value)
				{
					EnsureMappingExists(ctx, mapping.Key.Id, student.Id);
				}
			}
		}

		private static void EnsureMappingExists(EducationContainer ctx, int teacherId, int userId)
		{
			var teacher = ctx
				.Teachers
				.Where(t => t.teacherid == teacherId)
				.Single();

			var mappingExists = teacher
				.Students
				.Where(s => s.studentid == userId)
				.Any();

			if (!mappingExists)
			{
				var student = ctx
				.Students
				.Where(s => s.User.Id == userId)
				.Single();

				teacher.Students.Add(student);
				ctx.SaveChanges();
			}
		}
	}
}