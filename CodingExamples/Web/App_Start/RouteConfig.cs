﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Help Area",
				"ApiHelp/{controller}/{action}/{id}",
				new { controller = "Help", action = "Index", id = UrlParameter.Optional }
			).DataTokens = new RouteValueDictionary(new { area = "HelpPage" });

			routes.MapRoute(
				name: "Default",
				url: "{*catchall}",
				defaults: new { controller = "Default", action = "Index" }
			);
		}
	}
}