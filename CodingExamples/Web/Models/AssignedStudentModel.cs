﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
	public class AssignedStudentModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int GradeLevel { get; set; }
		public DateTime Birthdate { get; set; }
		public int TeacherId { get; set; }
		public int StudentId { get; set; }
	}
}