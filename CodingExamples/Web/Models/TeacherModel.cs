﻿namespace Web.Models
{
	public class TeacherModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int TeacherId { get; set; }
		public int UserId { get; set; }
	}
}