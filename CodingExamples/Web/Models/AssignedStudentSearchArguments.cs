﻿using System;

namespace Web.Models
{
	public class AssignedStudentSearchArguments
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int? GradeLevel { get; set; }
		public DateTime? Birthdate { get; set; }
		public bool? BirthdateGreater { get; set; }
	}
}